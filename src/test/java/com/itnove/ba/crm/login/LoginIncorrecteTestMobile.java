package com.itnove.ba.crm.login;

import com.itnove.ba.BaseSauceBrowserTest;
import com.itnove.ba.BaseTest;
import com.itnove.ba.crm.pages.LoginPage;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class LoginIncorrecteTestMobile extends BaseSauceBrowserTest {

    public void checkErrors(String user, String passwd){
        // S'introdueix l'usuari correcte.
        // S'introdueix la contrasenya incorrecte.
        // Es clicka  al botó de login. 
        LoginPage loginPage = new LoginPage(driver);
        loginPage.login(user, passwd);
        //Comprovar l'error
        assertTrue(loginPage.isErrorMessagePresentMobile(driver, wait));
        //assertEquals(loginPage.errorMessageDisplayedMobile(),
                //"You must specify a valid username and password.");
    }

    @Test
    public void testApp() throws InterruptedException {
        //User ok passw KO
        checkErrors("user","nami");
        //User KO passwd OK
        checkErrors("resu","bitnami");
        //User KO passwd KO
        checkErrors("resu","nami");
    }
}
