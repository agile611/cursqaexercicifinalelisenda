package com.itnove.ba.crm.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

/**
 * Created by guillem on 01/03/16.
 */
public class DashboardPage {

    private WebDriver driver;

    @FindBy(xpath = "(//*[@id=\"quickcreatetop\"]/ul/li[1]/a)[3]")
    public WebElement createAccountLink;

    @FindBy(xpath = "(//*[@id=\"quickcreatetop\"]/ul/li[5]/a)[3]")
    public WebElement createDocumentLink;

    @FindBy(xpath = "(//*[@id=\"quickcreatetop\"]/a)[3]")
    public WebElement createButton;

    @FindBy(id = "tab0")
    public WebElement suiteCrmDashboard;

    /*responsive identifier*/
    @FindBy(id = "xstab0")
    public WebElement suiteCrmDashboardMobile;

    @FindBy(xpath = "(.//*[@id='usermenucollapsed'])[3]")
    public WebElement icono;

    /*responsive identifier*/
    @FindBy(xpath = "(//*[@id=\"usermenucollapsed\"])[1]")
    public WebElement iconoMobile;

    @FindBy(xpath = ".//*[@id='tab-actions']/a")
    public WebElement actions;

    @FindBy(xpath = ".//*[@id='tab-actions']/ul/li[2]/input")
    public WebElement addTab;

    @FindBy(xpath = ".//*[@id='pagecontent']/div[3]/div/div/div[3]/button[2]")
    public WebElement addTabWidgetButton;

    @FindBy(xpath = "(.//*[@id='globalLinks']/ul)[2]")
    public WebElement menuIcono;

    @FindBy(xpath = "(//*[@id=\"logout_link\"])[3]")
    public WebElement logout;

    /*responsive identifier*/
    @FindBy(xpath = "(//*[@id=\"logout_link\"])[1]")
    public WebElement logoutMobile;

    @FindBy(xpath = "(//*[@id=\"searchbutton\"])[3]")
    public WebElement lupa;

    /*responsive identifier*/
    @FindBy(xpath = "(//*[@id=\"searchbutton\"])[1]")
    public WebElement lupaMobile;

    @FindBy(xpath = "(//*[@id=\"query_string\"])[5]")
    public WebElement searchBox;

    @FindBy(xpath = "(//*[@id=\"query_string\"])[1]")
    public WebElement searchBoxMobile;

    @FindBy(xpath = "(//*[@id=\"searchformdropdown\"]/div/span/button)[3]")
    public WebElement submitLupa;

    /*responsive identifier*/
    @FindBy(xpath = "(//*[@id=\"searchformdropdown\"]/div/span/button)[1]")
    public WebElement submitLupaMobile;

    @FindBy(id = "bigbutton")
    public WebElement botoLogin;

    public void logout(Actions hover) throws InterruptedException {
        hover.moveToElement(icono);
        hover.moveToElement(logout).click().build().perform();
    }

    public void logoutMobile() {
        iconoMobile.click();
        logoutMobile.click();
    }


    public void createButtonClick(Actions hover) throws InterruptedException {
        hover.moveToElement(createButton)
                .click().build().perform();
    }

    public void clickOnCreateAccountLink(Actions hover) throws InterruptedException {
        hover.moveToElement(createAccountLink).click().build().perform();

    }

    public void clickOnCreateDocumentLink(Actions hover) throws InterruptedException {
        hover.moveToElement(createButton)
                .moveToElement(createDocumentLink)
                .click().build().perform();
    }

    public void hoverAndClickEveryCreate(WebDriver driver, Actions hover) throws InterruptedException {
        createButtonClick(hover);
        String listElements = "(.//*[@id='quickcreatetop']/ul)[2]/li";
        String lsl = listElements + "/a";
        System.out.println(lsl);
        List<WebElement> listOfCreates = driver.findElements(By.xpath(lsl));
        for (int i = 1; i <= listOfCreates.size(); i++) {
            createButtonClick(hover);
            WebElement eachCreateItem = driver.findElement(By.xpath(listElements + "[" + i + "]/a"));
            hover.moveToElement(createButton)
                    .moveToElement(eachCreateItem)
                    .click().build().perform();
        }
    }

    public boolean isDashboardLoaded(WebDriver driver, WebDriverWait wait) {
        wait.until(ExpectedConditions.visibilityOf(suiteCrmDashboard));
        return suiteCrmDashboard.isDisplayed();
    }

    public boolean isDashboardLoadedMobile(WebDriver driver, WebDriverWait wait) {
        wait.until(ExpectedConditions.visibilityOf(suiteCrmDashboardMobile));
        return suiteCrmDashboardMobile.isDisplayed();
    }


    public void addTab(WebDriverWait wait, Actions hover) throws InterruptedException {
        hover.moveToElement(actions)
                .moveToElement(actions)
                .click().build().perform();
        hover.moveToElement(actions)
                .moveToElement(addTab)
                .click().build().perform();
        wait.until(ExpectedConditions.visibilityOf(addTabWidgetButton));
        hover.moveToElement(addTabWidgetButton)
                .moveToElement(addTabWidgetButton)
                .click().build().perform();
    }

    public void search(Actions hover, String keyword) throws InterruptedException {

        hover.moveToElement(lupa)
                .moveToElement(lupa)
                .click().build().perform();
        searchBox.click();
        searchBox.sendKeys(keyword);
        hover.moveToElement(submitLupa)
                .moveToElement(submitLupa)
                .click().build().perform();

    }

    public void searchMobile(String keyword) throws InterruptedException {

        lupaMobile.click();
        searchBoxMobile.click();
        searchBoxMobile.sendKeys(keyword);
        submitLupaMobile.click();

    }

    public DashboardPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

}