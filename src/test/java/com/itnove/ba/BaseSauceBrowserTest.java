package com.itnove.ba;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.remote.MobileBrowserType;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

/**
 * Created by Elisenda on 8/12/2017
 */
public class BaseSauceBrowserTest {
    public RemoteWebDriver driver;
    public WebDriverWait wait;
    public Actions hover;
    public int timeOut = 10;

    @BeforeMethod
    public void setUp() throws MalformedURLException {
        String device = System.getProperty("device");
        // switch between diffrent browsers, e.g. iOS Safari or Android Chrome
        // let's use the os name to differentiate, because we only use default browser in that os
        if (device != null && device.equalsIgnoreCase("android")) {
            DesiredCapabilities caps = new DesiredCapabilities();
            caps.setCapability("appiumVersion", "1.7.1");
            caps.setCapability("deviceName","Samsung Galaxy S4 Emulator");
            caps.setCapability("deviceOrientation", "portrait");
            caps.setCapability("browserName", "Browser");
            caps.setCapability("platformVersion", "4.4");
            caps.setCapability("platformName","Android");

            driver =
        new AndroidDriver(new URL("http://itnove:4394d787-3244-4c03-9490-3816f2bb683b@ondemand.saucelabs.com:80/wd/hub"),
                caps);
        } else {
            DesiredCapabilities caps = DesiredCapabilities.iphone();
            caps.setCapability("appiumVersion", "1.7.1");
            caps.setCapability("deviceName", "iPhone 6 Plus Simulator");
            caps.setCapability("deviceOrientation", "portrait");
            caps.setCapability("platformVersion", "10.0");
            caps.setCapability("platformName", "iOS");
            caps.setCapability("browserName", "Safari");
            driver = new IOSDriver(new URL("http://itnove:4394d787-3244-4c03-9490-3816f2bb683b@ondemand.saucelabs.com:80/wd/hub"), caps);
        }
        wait = new WebDriverWait(driver, 10);
        hover = new Actions(driver);
        //Accedir a pagina
        if(this.getClass().getCanonicalName().contains("crm")){
            driver.navigate().to("http://crm.votarem.lu");}

        else if (this.getClass().getCanonicalName().contains("opencart")&& this.getClass().getCanonicalName().contains("admin")){
            driver.navigate().to("http://opencart.votarem.lu/admin");}

        else if (this.getClass().getCanonicalName().contains("opencart")){
            driver.navigate().to("http://opencart.votarem.lu");}

        else {driver.navigate().to("http://wordpress.votarem.lu/");}

        driver.manage().timeouts().pageLoadTimeout(timeOut, TimeUnit.SECONDS);
        driver.manage().timeouts().setScriptTimeout(timeOut, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(timeOut, TimeUnit.SECONDS);
    }

    @AfterMethod
    public void tearDown() {
        if (driver != null) {
            driver.quit();
        }
    }
}
