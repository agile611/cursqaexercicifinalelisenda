package com.itnove.ba;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.remote.MobileBrowserType;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.concurrent.TimeUnit;


/**
 * Created by guillem on 29/02/16.
 */
public class BaseTest {
    public RemoteWebDriver driver;
    public WebDriverWait wait;
    public Actions hover;
    public int timeOut = 10;


    @BeforeMethod
    public void setUp() throws IOException {
        String browser = System.getProperty("browser");
        if (browser != null && browser.equalsIgnoreCase("firefox")) {
            DesiredCapabilities capabilities = DesiredCapabilities.firefox();
           // System.setProperty("webdriver.gecko.driver", "src" + File.separator + "main" + File.separator + "resources" + File.separator + "geckodriver-linux");
            System.setProperty("webdriver.gecko.driver", "src" + File.separator + "main" + File.separator + "resources" + File.separator + "geckodriver.exe");
            driver = new FirefoxDriver(capabilities);
        } else {
            DesiredCapabilities capabilities = DesiredCapabilities.chrome();
          // System.setProperty("webdriver.chrome.driver", "src" + File.separator + "main" + File.separator + "resources" + File.separator + "chromedriver-linux");
            System.setProperty("webdriver.chrome.driver", "src" + File.separator + "main" + File.separator + "resources" + File.separator + "chromedriver.exe");
            driver = new ChromeDriver(capabilities);
        }
        wait = new WebDriverWait(driver, timeOut);
        hover = new Actions(driver);
        driver.manage().deleteAllCookies();
        driver.manage().window().fullscreen();
        driver.manage().timeouts().pageLoadTimeout(timeOut, TimeUnit.SECONDS);
        driver.manage().timeouts().setScriptTimeout(timeOut, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(timeOut, TimeUnit.SECONDS);
        //Accedir a pagina

        if(this.getClass().getCanonicalName().contains("crm")){
            driver.navigate().to("http://crm.votarem.lu");}

        else if (this.getClass().getCanonicalName().contains("opencart")&& this.getClass().getCanonicalName().contains("admin")){
            driver.navigate().to("http://opencart.votarem.lu/admin");}

        else if (this.getClass().getCanonicalName().contains("opencart")){
            driver.navigate().to("http://opencart.votarem.lu");}

        else {driver.navigate().to("http://wordpress.votarem.lu/");}
    }


    @AfterMethod
    public void tearDown() {
        driver.quit();
    }

}
