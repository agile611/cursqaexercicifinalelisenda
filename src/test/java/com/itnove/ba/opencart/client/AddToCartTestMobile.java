package com.itnove.ba.opencart.client;

import com.itnove.ba.BaseSauceBrowserTest;
import com.itnove.ba.BaseTest;
import com.itnove.ba.opencart.client.pages.CheckoutPage;
import com.itnove.ba.opencart.client.pages.HomePage;
import org.testng.annotations.Test;

import static org.testng.Assert.assertTrue;

public class AddToCartTestMobile extends BaseSauceBrowserTest {

    @Test
    public void testApp() throws InterruptedException {

        HomePage myHome = new HomePage(driver);
        myHome.addCartMobile();

        CheckoutPage mycheckout= new CheckoutPage(driver);

        assertTrue(mycheckout.visibilityOfCheckoutMobile(wait));


    }
}
