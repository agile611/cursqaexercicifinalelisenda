package com.itnove.ba.opencart.client.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import static org.openqa.selenium.support.ui.ExpectedConditions.visibilityOf;


public class SearchResultsPage {

    private WebDriver driver;

    @FindBy(xpath = "//*[@id=\"product-search\"]/ul/li[2]/a")
    public WebElement searchDashboard;

    @FindBy(xpath = "//*[@id=\"content\"]/p[2]")
    public WebElement termNotFound;

    @FindBy(xpath = "//*[@id=\"content\"]/div[3]/div/div")
    public WebElement productoBarra;


    public String termNot(){
        return termNotFound.getText();
    }

    public boolean isDashboardSearchLoaded(WebDriverWait wait){
        wait.until(visibilityOf(searchDashboard));
        return searchDashboard.isDisplayed();
    }

    public boolean isProductoBarraDisplayed(WebDriverWait wait){
        wait.until(visibilityOf(productoBarra));
        return productoBarra.isDisplayed();
    }


    public SearchResultsPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

}
