package com.itnove.ba.opencart.client.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by guillem on 01/03/16.
 */
public class CheckoutPage {

    private WebDriver driver;


    @FindBy(id = "checkout-checkout")
    public WebElement checkoutLoaded;

    @FindBy(id = "button-account")
    public WebElement continueButton;

    @FindBy(xpath = "//*[@id=\"collapse-payment-address\"]")
    public WebElement personalDetails;

    @FindBy(xpath = "//*[@id=\"input-payment-firstname\"]")
    public WebElement nameInput;

    @FindBy(xpath = "//*[@id=\"input-payment-lastname\"]")
    public WebElement surnameInput;

    @FindBy(xpath = "//*[@id=\"input-payment-email\"]")
    public WebElement emailInput;

    @FindBy (xpath = "//*[@id=\"input-payment-telephone\"]")
    public WebElement telephoneInput;

    @FindBy (xpath = "//*[@id=\"content\"]/p")
    public WebElement messageEmpty;

    @FindBy(xpath = "//*[@id=\"input-payment-password\"]")
    public WebElement passwordInput;

    @FindBy(xpath = "//*[@id=\"input-payment-confirm\"]")
    public WebElement passwordInput2;

    @FindBy(xpath = "//*[@id=\"checkout-cart\"]/ul")
    public WebElement cartOkMobile;

    public boolean visibilityOfCheckout(WebDriverWait wait){

     wait.until(ExpectedConditions.visibilityOf(checkoutLoaded));
     return checkoutLoaded.isDisplayed();
    }

    public String getMessageEmpty(){
        return messageEmpty.getText();
    }

    public boolean visibilityOfCheckoutMobile(WebDriverWait wait){

        wait.until(ExpectedConditions.visibilityOf(cartOkMobile));
        return cartOkMobile.isDisplayed();
    }



   public void clickContinueButton(WebDriver driver, Actions hover, WebDriverWait wait) throws InterruptedException{

       driver.navigate().refresh(); //per afegirli un refresh em de passar Webdriver driver com a parámetre!!
       Thread.sleep(300);
       hover.moveToElement(continueButton).moveToElement(continueButton).click().build().perform();
       continueButton.click();
       continueButton.click();
       wait.until(ExpectedConditions.visibilityOf(personalDetails));

    }

    public void fillFields(String name, String apellido,  String email, String telephone, String password, String password2) {

        nameInput.sendKeys(name);
        nameInput.sendKeys(apellido);
        emailInput.sendKeys(email);
        telephoneInput.sendKeys(telephone);
        passwordInput.sendKeys(password);
        nameInput.sendKeys(password2);
        //Thread.sleep(300);

    }


    public CheckoutPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

}
