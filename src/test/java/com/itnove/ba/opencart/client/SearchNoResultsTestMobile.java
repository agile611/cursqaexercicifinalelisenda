package com.itnove.ba.opencart.client;

import com.itnove.ba.BaseSauceBrowserTest;
import com.itnove.ba.opencart.client.pages.HomePage;
import com.itnove.ba.opencart.client.pages.SearchResultsPage;
import org.testng.annotations.Test;

import static org.testng.Assert.assertTrue;

public class SearchNoResultsTestMobile extends BaseSauceBrowserTest {

    @Test
    public void testApp() throws InterruptedException {

        HomePage myHome = new HomePage(driver);
        myHome.search("caracola");
        SearchResultsPage mySearchResults= new SearchResultsPage(driver);
        assertTrue(mySearchResults.isDashboardSearchLoaded(wait));

        // String recojo= mySearchResults.termNot();
        // System.out.print(recojo);
        mySearchResults.termNot().contains("empty");


    }
}
