package com.itnove.ba.opencart.client.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static org.junit.Assert.assertTrue;


public class AccountPage {

    private WebDriver driver;


    @FindBy(xpath = "//*[@id=\"input-firstname\"]")
    public WebElement firstNameInput;

    @FindBy(xpath = "//*[@id=\"input-lastname\"]")
    public WebElement surnameInput;

    @FindBy(xpath = "//*[@id=\"input-email\"]")
    public WebElement emailInput;

    @FindBy (xpath = "//*[@id=\"input-telephone\"]")
    public WebElement telephoneInput;

    @FindBy(xpath = "//*[@id=\"input-password\"]")
    public WebElement passwordInput;

    @FindBy(xpath = "//*[@id=\"input-confirm\"]")
    public WebElement passwordInput2;

    @FindBy(xpath = "//*[@id=\"content\"]/form/div/div/input[1]")
    public WebElement checkboxInput;

    @FindBy(xpath = "//*[@id=\"content\"]/form/div/div/input[2]")
    public WebElement botonCrear;

    @FindBy(xpath = "//*[@id=\"content\"]/p[1]")
    public WebElement congratMessage;

    @FindBy(xpath = "//*[@id=\"content\"]/form/fieldset[2]/div[2]/div/div")
    public WebElement passwordsDontMatch;




    public void fillFields(String name, String apellido,
                           String email, String telephone, String password, String password2) {

        firstNameInput.sendKeys(name);
        surnameInput.sendKeys(apellido);
        emailInput.sendKeys(email);
        telephoneInput.sendKeys(telephone);
        passwordInput.sendKeys(password);
        passwordInput2.sendKeys(password2);
        //Thread.sleep(300);

    }

    public void checkPrivacyPolicy(){

        checkboxInput.click();
    }

    public void clickCreateAccount(){

        botonCrear.click();
    }

  public String congratulationsAccount(){
        System.out.println(congratMessage.getText());
        return congratMessage.getText();
  }

    public String getMessagePasswordsDontMatch(){

        return passwordsDontMatch.getText();
    }


    public AccountPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

}
