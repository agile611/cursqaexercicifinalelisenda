package com.itnove.ba.opencart.client;

import com.itnove.ba.BaseTest;
import com.itnove.ba.opencart.client.pages.CheckoutPage;
import com.itnove.ba.opencart.client.pages.HomePage;
import org.testng.annotations.Test;

import static org.testng.Assert.assertTrue;

/* Test if we click checkout without having any item in the trolley we get a message saying
the trolley is empty
 */
public class CheckoutEmptyTrolleyTest extends BaseTest {

    @Test
    public void testApp() throws InterruptedException {

        HomePage myHome = new HomePage(driver);
        myHome.checkoutEmpty();
        CheckoutPage myCheckout = new CheckoutPage(driver);
       //yCheckout.visibilityOfCheckout(wait);
      //assertTrue(myCheckout.visibilityOfCheckout(wait));
        assertTrue(myCheckout.getMessageEmpty().contains("empty"));

    }
}
