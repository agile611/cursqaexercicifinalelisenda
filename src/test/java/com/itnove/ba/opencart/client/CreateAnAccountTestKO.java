package com.itnove.ba.opencart.client;

import com.itnove.ba.BaseTest;
import com.itnove.ba.opencart.client.pages.AccountPage;
import com.itnove.ba.opencart.client.pages.HomePage;
import org.apache.commons.lang3.RandomStringUtils;
import org.testng.annotations.Test;

import java.util.Random;
import java.util.UUID;

import static org.testng.Assert.assertTrue;

/*This test is to make sure NO account is created when the two passwords don't match */

public class CreateAnAccountTestKO extends BaseTest {

    @Test
    public void testApp() throws InterruptedException {

        String primero= generateRandomStringMaxLenght();
        String segundo= generateRandomStringMaxLenght();
        String tercero=generateRandomStringMaxLenght()+"@"+generateRandomStringMaxLenght()+".com";
        String cuarto=generateRandomStringMaxLenght();
        String quinto=generateRandomStringMaxLenght();
        String sexto=generateRandomStringMaxLenght();


        HomePage myHome = new HomePage(driver);
        myHome.createAccount();

        AccountPage myAccount = new AccountPage(driver);
        myAccount.fillFields(primero, segundo, tercero, cuarto, quinto, sexto);

        Thread.sleep(2000);

        myAccount.checkPrivacyPolicy();
        Thread.sleep(2000);
        myAccount.clickCreateAccount();
        Thread.sleep(2000);

        assertTrue(myAccount.getMessagePasswordsDontMatch().contains("match"));

    }


    public String generateRandomStringMaxLenght() {

        int leftLimit = 97; // letter 'a'
        int rightLimit = 122; // letter 'z'
        int targetStringLength = 8;
        Random random = new Random();
        StringBuilder buffer = new StringBuilder(targetStringLength);
        for (int i = 0; i < targetStringLength; i++) {
            int randomLimitedInt = leftLimit + (int)
                    (random.nextFloat() * (rightLimit - leftLimit + 1));
            buffer.append((char) randomLimitedInt);
        }
        String generatedString = buffer.toString();

            return generatedString;
    }

}
