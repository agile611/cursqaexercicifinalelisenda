package com.itnove.ba.opencart.client;

import com.itnove.ba.BaseTest;
import com.itnove.ba.opencart.client.pages.CheckoutPage;
import com.itnove.ba.opencart.client.pages.HomePage;
import org.testng.annotations.Test;

import static org.testng.Assert.assertTrue;

public class AddToCartTest extends BaseTest {

    @Test
    public void testApp() throws InterruptedException {

        HomePage myHome = new HomePage(driver);
        myHome.addCartFromHome(driver, wait, hover);
        CheckoutPage myCheckout = new CheckoutPage(driver);
        myCheckout.visibilityOfCheckout(wait);
        myCheckout.clickContinueButton(driver, hover, wait);
        myCheckout.visibilityOfCheckout(wait);
        assertTrue(myCheckout.visibilityOfCheckout(wait));



    }
}
