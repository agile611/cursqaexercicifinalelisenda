package com.itnove.ba.opencart.client;

import com.itnove.ba.BaseTest;
import com.itnove.ba.opencart.client.pages.HomePage;
import com.itnove.ba.opencart.client.pages.SearchResultsPage;
import org.testng.annotations.Test;

import static org.testng.Assert.assertTrue;

public class SearchCorrecteTest extends BaseTest {

    @Test
    public void testApp() throws InterruptedException {

        HomePage myHome = new HomePage(driver);
        myHome.search("iPhone");
        SearchResultsPage mySearchResults= new SearchResultsPage(driver);
        assertTrue(mySearchResults.isDashboardSearchLoaded(wait));
        assertTrue(mySearchResults.isProductoBarraDisplayed(wait));

    }
}
