package com.itnove.ba.opencart.client.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static org.openqa.selenium.support.ui.ExpectedConditions.visibilityOf;


public class HomePage {

    private WebDriver driver;

    @FindBy(xpath = "//*[@id=\"search\"]/input")
    public WebElement searchInput;

    @FindBy(xpath = "//*[@id=\"search\"]/span/button")
    public WebElement searchSubmit;

    @FindBy(xpath = "//*[@id=\"content\"]/div[2]/div[1]/div/div[3]/button[1]")
    public WebElement firstProductAddCartButton;

    @FindBy(xpath = "//*[@id=\"cart\"]/button")
    public WebElement checkoutButton;

    @FindBy(xpath = "//*[@id=\"content\"]/div[2]/div[1]/div/div[3]/button[1]")
    public WebElement checkoutButtonMobile;

    @FindBy(xpath = "//*[@id=\"common-home\"]/div[1]/a[2]")
    public WebElement goToCartMobile;

    @FindBy(id = "cart-total")
    public WebElement checkoutTotal;

    @FindBy(xpath = "//*[@id=\"cart\"]/ul/li[2]/div/p/a[2]")
    public WebElement checkoutLink;

    @FindBy(xpath = ".//*[contains(@class,'alert')]")
    public WebElement alertBanner;

    @FindBy(xpath = "//*[@id=\"top-links\"]/ul/li[5]/a")
    public WebElement checkOutLinkEmpty;

    @FindBy(xpath = "//*[@id=\"top-links\"]/ul/li[2]/a")
    public WebElement createAccountButton;

    @FindBy(xpath = "//*[@id=\"top-links\"]/ul/li[2]/ul/li[1]/a")
    public WebElement registerButton;

    public void createAccount(){

            createAccountButton.click();
            registerButton.click();

    }

    public void addCartMobile(){

        checkoutButtonMobile.click();
        goToCartMobile.click();
    }


    public void addCartFromHome(WebDriver driver, WebDriverWait wait, Actions hover) throws InterruptedException {
        firstProductAddCartButton.click();
        wait.until(ExpectedConditions.visibilityOf(alertBanner));
        wait.until(ExpectedConditions.visibilityOf(checkoutButton));
        wait.until(ExpectedConditions.visibilityOf(checkoutTotal));
        driver.navigate().refresh(); //per afegirli un refresh em de passar Webdriver driver com a parámetre!!
        hover.moveToElement(checkoutTotal).moveToElement(checkoutTotal).click().build().perform();
        wait.until(ExpectedConditions.visibilityOf(checkoutLink));
        hover.moveToElement(checkoutLink).click().build().perform();
    }

    public void search(String term) {
        searchInput.click();
        searchInput.sendKeys(term);
        searchSubmit.click();
    }

    public void checkoutEmpty(){
        checkOutLinkEmpty.click();

    }

    public HomePage(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

}

