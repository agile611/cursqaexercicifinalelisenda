package com.itnove.ba.opencart.client;

import com.itnove.ba.BaseTest;
import com.itnove.ba.opencart.client.pages.AccountPage;
import com.itnove.ba.opencart.client.pages.CheckoutPage;
import com.itnove.ba.opencart.client.pages.HomePage;
import junit.framework.TestCase;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import java.util.Random;
import java.util.UUID;

import static org.junit.Assert.assertNull;
import static org.testng.Assert.assertTrue;

public class CrateAnAccountTest extends BaseTest {

    @Test
    public void testApp() throws InterruptedException {

            String primero= generateRandomStringMaxLenght();
            String segundo= generateRandomStringMaxLenght();
            String tercero=generateRandomStringMaxLenght()+"@"+generateRandomStringMaxLenght()+".com";
            String cuarto=generateRandomStringMaxLenght();
            String quinto="password";
            String sexto="password";


        HomePage myHome = new HomePage(driver);
        myHome.createAccount();
        AccountPage myAccount = new AccountPage(driver);
        myAccount.fillFields(primero, segundo, tercero, cuarto, quinto, sexto);
        myAccount.checkPrivacyPolicy();
       //Thread.sleep(1000);
        myAccount.clickCreateAccount();
        //Thread.sleep(1000);
        myAccount.congratulationsAccount();
       assertTrue(myAccount.congratulationsAccount().contains("Congratulations"));

    }

    public String generateRandomStringMaxLenght() {

        int leftLimit = 97; // letter 'a'
        int rightLimit = 122; // letter 'z'
        int targetStringLength = 8;
        Random random = new Random();
        StringBuilder buffer = new StringBuilder(targetStringLength);
        for (int i = 0; i < targetStringLength; i++) {
            int randomLimitedInt = leftLimit + (int)
                    (random.nextFloat() * (rightLimit - leftLimit + 1));
            buffer.append((char) randomLimitedInt);
        }
        String generatedString = buffer.toString();

        return generatedString;
    }
}
