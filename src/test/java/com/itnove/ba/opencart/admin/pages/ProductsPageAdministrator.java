package com.itnove.ba.opencart.admin.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;


public class ProductsPageAdministrator {

    private WebDriver driver;

    @FindBy(xpath = "//*[@id=\"form-product\"]/ul/li[2]/a")
    public WebElement data;

    @FindBy(xpath = "//*[@id=\"input-name1\"]")
    public WebElement inputProducto;

    @FindBy(xpath = "//*[@id=\"input-model\"]")
    public WebElement inputModel;

    @FindBy(xpath = "//*[@id=\"input-meta-title1\"]")
    public WebElement inputProductoMeta;

    @FindBy(xpath = "//*[@id=\"content\"]/div[1]/div/div/button")
    public WebElement save;

    @FindBy(xpath = "//*[@id=\"content\"]/div[2]/div[1]/i")
    public WebElement alertCreado;


    public void createProduct(WebDriver driver, String nombre, String nombre2, String nombre3) throws InterruptedException {

        inputProducto.sendKeys(nombre);
        inputProductoMeta.sendKeys(nombre2);
        data.click();
        inputModel.sendKeys(nombre3);
        save.click();

    }

    public String creationMessageDisplayed(){ return alertCreado.getText();
    }


    public ProductsPageAdministrator(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

}