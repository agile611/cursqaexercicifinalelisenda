package com.itnove.ba.opencart.admin.tests;

import com.itnove.ba.BaseTest;
import com.itnove.ba.opencart.admin.pages.LoginPageAdministrator;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class LoginIncorrecteTest extends BaseTest {

    public void checkErrors(String user, String passwd){

        LoginPageAdministrator myLogin = new LoginPageAdministrator(driver);
        myLogin.login(user, passwd);
        //Comprovar l'error
        assertTrue(myLogin.isErrorMessageShown(driver, wait));

    }

    @Test
    public void testApp() throws InterruptedException {
        //User ok passw KO
        checkErrors("user","nami");
        //User KO passwd OK
        checkErrors("resu","bitnami");
        //User KO passwd KO
        checkErrors("resu","nami");
    }
}
