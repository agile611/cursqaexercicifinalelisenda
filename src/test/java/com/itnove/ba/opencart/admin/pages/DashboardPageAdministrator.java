package com.itnove.ba.opencart.admin.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;


public class DashboardPageAdministrator {

    private WebDriver driver;

    @FindBy(id = "modal-security")
    public WebElement dashboardBox;

    @FindBy(xpath = "//*[@id=\"content\"]/div[1]/div/div/a")
    public WebElement addNew;

    @FindBy(xpath = "//*[@id=\"menu-catalog\"]/a")
    public WebElement catalog;

    @FindBy(xpath = "//*[@id=\"modal-security\"]/div/div/div[1]/button")
    public WebElement close;

    @FindBy(xpath = "//*[@id=\"collapse1\"]/li[2]/a")
    public WebElement products;

    @FindBy(xpath = "//*[@id=\"button-menu\"]")
    public WebElement menuResponsive;




    public boolean isDashboardBoxShown(WebDriver driver, WebDriverWait wait){

        wait.until(ExpectedConditions.visibilityOf(dashboardBox));
        return dashboardBox.isDisplayed();
    }

    public void addElements () {
                addNew.click();
    }


    public void closeModal () {
        close.click();
    }

    public void clickMenuResponsive() {
                menuResponsive.click();
    }

    public void hoverAndAddSeveralElements(WebDriver driver, Actions hover) throws InterruptedException {

        closeModal();
        catalog.click();
        String listElements = "//*[@id=\"collapse1\"]/li";
        String lsl = listElements + "/a";
        System.out.println(lsl);
        List<WebElement> listOfCreates = driver.findElements(By.xpath(lsl));
        for (int i = 1; i <= listOfCreates.size(); i++) {
            WebElement eachCreateItem = driver.findElement(By.xpath(listElements + "[" + i + "]/a"));
            hover.moveToElement(eachCreateItem)
                    .moveToElement(eachCreateItem)
                    .click().build().perform();
                          addElements();
        }
    }

    public void hoverAndAddSeveralElementsMobile(WebDriver driver) throws InterruptedException {
        clickMenuResponsive();
        close.click();
        catalog.click();
        String listElements = "//*[@id=\"collapse1\"]/li";
        String lsl = listElements + "/a";
        //System.out.println(lsl);
        List<WebElement> listOfCreates = driver.findElements(By.xpath(lsl));
        for (int i = 1; i <= listOfCreates.size(); i++) {
            WebElement eachCreateItem = driver.findElement(By.xpath(listElements + "[" + i + "]/a"));
      eachCreateItem.click();
                    addElements();
        }
    }

    public void hoverClickProduct(){

        catalog.click();
        products.click();
    }


    public DashboardPageAdministrator(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

}