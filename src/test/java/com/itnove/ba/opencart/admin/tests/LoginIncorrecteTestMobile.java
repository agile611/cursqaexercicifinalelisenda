package com.itnove.ba.opencart.admin.tests;

import com.itnove.ba.BaseSauceBrowserTest;
import com.itnove.ba.opencart.admin.pages.LoginPageAdministrator;
import org.testng.annotations.Test;

import static org.testng.Assert.assertTrue;

public class LoginIncorrecteTestMobile extends BaseSauceBrowserTest {

    public void checkErrors(String user, String passwd){

        LoginPageAdministrator myLogin = new LoginPageAdministrator(driver);
        myLogin.login(user, passwd);
        //Comprovar l'error
        assertTrue(myLogin.isErrorMessageShown(driver, wait));

    }

    @Test
    public void testApp() throws InterruptedException {
        //User ok passw KO
        checkErrors("user","nami");
        //User KO passwd OK
        //checkErrors("resu","bitnami");
        //User KO passwd KO
       // checkErrors("resu","nami");
    }
}
