package com.itnove.ba.opencart.admin.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;


public class LoginPageAdministrator {

    private WebDriver driver;

    @FindBy(id = "input-username")
    public WebElement username;

    @FindBy(id = "input-password")
    public WebElement password;

    @FindBy(xpath ="//*[@id=\"content\"]/div/div/div/div/div[2]/form/div[3]/button")
    public WebElement botoLogin;

    @FindBy(xpath="//*[@id=\"content\"]/div/div/div/div/div[2]/div")
    public WebElement errorMessage;

    public void login(String user, String passwd){

        username.sendKeys(user);

        password.sendKeys(passwd);
        botoLogin.click();
    }

    public boolean isErrorMessageShown(WebDriver driver, WebDriverWait wait){

        wait.until(ExpectedConditions.visibilityOf(errorMessage));
        return errorMessage.isDisplayed();
    }


    public LoginPageAdministrator(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

}