package com.itnove.ba.opencart.admin.tests;

import com.itnove.ba.BaseTest;
import com.itnove.ba.opencart.admin.pages.DashboardPageAdministrator;
import com.itnove.ba.opencart.admin.pages.LoginPageAdministrator;
import com.itnove.ba.opencart.admin.pages.ProductsPageAdministrator;
import org.testng.annotations.Test;

import java.util.UUID;

import static org.testng.Assert.assertEquals;

public class CreationProductsTest extends BaseTest {
    String usuarioTexto = "user";
    String passwordTexto = "bitnami1";
    public String accountName = UUID.randomUUID().toString();
    public String accountName2 = UUID.randomUUID().toString();
    public String accountName3 = UUID.randomUUID().toString();

    @Test
    public void testApp() throws InterruptedException {
        LoginPageAdministrator myLogin = new LoginPageAdministrator(driver);
        myLogin.login(usuarioTexto, passwordTexto);
        DashboardPageAdministrator myDashboard = new DashboardPageAdministrator(driver);
        myDashboard.isDashboardBoxShown(driver, wait);
        myDashboard.closeModal();
        myDashboard.hoverClickProduct();
        myDashboard.addElements();
        ProductsPageAdministrator myProducts = new ProductsPageAdministrator(driver);
        myProducts.createProduct(driver, accountName, accountName2, accountName3);
        myProducts.creationMessageDisplayed().contains("Success");


    }
}
