package com.itnove.ba.opencart.admin.tests;

import com.itnove.ba.BaseSauceBrowserTest;
import com.itnove.ba.opencart.admin.pages.DashboardPageAdministrator;
import com.itnove.ba.opencart.admin.pages.LoginPageAdministrator;
import org.testng.annotations.Test;

public class LoginCorrecteTestMobile extends BaseSauceBrowserTest {
    String usuarioTexto="user";
    String passwordTexto="bitnami1";

    @Test
    public void testApp() throws InterruptedException {
        LoginPageAdministrator myLogin= new LoginPageAdministrator(driver);
        myLogin.login(usuarioTexto, passwordTexto);
        DashboardPageAdministrator myDashboard= new DashboardPageAdministrator(driver);
        myDashboard.isDashboardBoxShown(driver, wait);
    }
}
